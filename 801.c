#include <stdio.h>
struct student{
	char firstname[50];
	char subject[50];
	float marks;
} s[10];

int main() {
	int i;
	printf("Enter information of students;\n");
	for(i=0;i<5;++i){
		printf("Enter First Name:");
		scanf("%s", s[i].firstname);
		printf("Enter Subject:");
		scanf("%s", s[i].subject);
		printf("Enter Marks:");
		scanf("%f", &s[i].marks);
	}
	printf("Displaying information entered:\n");
	for(i=0;i<5;++i){
		printf("First Name: %s\n",s[i].firstname);
		printf("Subject: %s\n", s[i].subject);
		printf("Marks: %.2f\n", s[i].marks);
	}
	return 0;
}