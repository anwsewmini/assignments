#include<stdio.h> 

void read(int m1[10][10], int m2[10][10], int r1, int c1, int r2, int c2);
void multiply(int m1[10][10], int m2[10][10], int mult[10][10], int r1, int c1, int r2, int c2);
void add(int m1[10][10], int m2[10][10], int sum[10][10],int r, int c);
void print(int m[10][10],int r, int c);

int main()
{
	int m1[10][10], m2[10][10],mult[10][10],sum[10][10],r1, c1, r2, c2;

	printf("Enter the number of rows and columns for the first matrix\n");
	scanf("%d %d",&r1,&c1);

	printf("Enter the number of rows and columns for the second matrix\n");
	scanf("%d %d",&r2,&c2);

	if(r1==c1 && r1==r2 && r1==c2 ){

		read(m1,m2,r1,c1,r2,c2);

		printf("When the 2 matrices are added:\n");
		add(m1,m2,sum,r1,c1);
		print(sum,r1,c1);

		printf("When the 2 matrices are multiplied:\n");
		multiply(m1,m2,mult,r1,r2,c1,c2);
		print(mult,r1,c2);

	}

	else{
	
	if(c1==r2){
	
		read(m1,m2,r1,c1,r2,c2);
		multiply(m1,m2,mult,r1,r2,c1,c2);

		printf("When the two matrices are multiplied:\n");
		print(mult,r1,c2);

	}

	if(r1==r2 && c1==c2){
	
		read(m1,m2,r1,c1,r2,c2);
		add(m1,m2,sum,r1,c1);

		printf("When the two matrices are added:\n");
		print(sum,r1,c1);

	}

	if(c1!=r2){
		printf("It is not possible to multiply the two matrices\n");
	}


	if(r1!=r2 || c1!=c2){
		printf("It is not possible to add the two matrices\n");

	}

	}

	return 0;
}

/*function to read two input matrices from the user*/
void read(int m1[10][10], int m2[10][10], int r1, int c1, int r2, int c2){

	printf("Enter the values for the first matrix\n");
	for(int a=0; a<r1; a++){
		for(int b=0; b<c1; b++){
			printf("Enter data in [%d][%d]:",a+1,b+1);
			scanf("%d", &m1[a][b]);
		}
		printf("\n");
	}

	printf("Enter the values for the second matrix\n");
	for(int i=0; i<r2; i++){
		for(int j=0; j<c2; j++){
			printf("Enter data in [%d][%d]:",i+1,j+1);
			scanf("%d", &m2[i][j]);
		}
		printf("\n");
	}

}

/*function to add the two matrices and store the value*/
void add(int m1[10][10], int m2[10][10], int sum[10][10], int r, int c){
	/*initialize elements of matrix sum to zero*/
	for(int i=0; i<r; i++){
		for (int j=0; j<c; j++){
			sum[i][j]=0;
		}
	}

	/*adding m1 and m2 and storing the value in sum*/
	for (int i=0; i<r; i++){
		for (int j=0; j<c; j++){
				sum[i][j]=m1[i][j]+m2[i][j];
			}
		}
}
	

/*function to multiply the two matrices and store the value*/
void multiply(int m1[10][10], int m2[10][10], int mult[10][10], int r1, int c1, int r2, int c2){
	/*initialize elements of matrix mult to zero*/
	for(int i=0; i<r1; i++){
		for (int j=0; j<c2; j++){
			mult[i][j]=0;
		}
	}

	/*multiplying m1 and m2 and storing the value in mult*/
	for (int i=0; i<r1; i++){
		for (int j=0; j<c2; j++){
			for(int k=0; k<c1; k++){
				mult[i][j]+=m1[i][k]*m2[k][j];
			}
		}
	}
}

/*function to print the resultant matrix*/
void print(int m[10][10], int r, int c){

	for(int a=0; a<r; a++){
		for(int b=0; b<c; b++){
			printf("%d ", m[a][b]);
		}
		printf("\n");
	}
	printf("\n");
}
